import unittest
from unittest import mock
import main.guess_number as guess_number

class TestGuessNumber(unittest.TestCase):

    #test guess_int
    @mock.patch('random.randint',return_value=int(7))
    def test_guess_int_0_to_10_should_be_7(self, guessValue):
        result = guess_number.guess_int(0,10)
        self.assertEqual(result,7,"should be 7")
        
    @mock.patch('random.randint',return_value=int(167))
    def test_guess_int_10_to_1000_should_be_167(self, guessValue):
        result = guess_number.guess_int(10,1000)
        self.assertEqual(result,167,"should be 167")

    @mock.patch('random.randint',return_value=int(17000))
    def test_guess_int_1000_to_100000_should_be_17000(self, guessValue):
        result = guess_number.guess_int(1000,10000000)
        self.assertEqual(result,17000,"should be 17000")
    
    @mock.patch('random.randint',return_value=int(76))
    def test_guess_int_200_to_1_should_be_76(self, guessValue):
        result = guess_number.guess_int(200,1)
        self.assertEqual(result,76,"should be 76")

    @mock.patch('random.randint',return_value=int(0))
    def test_guess_int_0_to_0_should_be_0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0,"should be 0")

    @mock.patch('random.randint',return_value=int(167))
    def test_guess_int_167_to_167_should_be_167(self, guessValue):
        result = guess_number.guess_int(167,167)
        self.assertEqual(result,167,"should be 167")
    
    @mock.patch('random.randint',return_value=int(-999))
    def test_guess_int_minus999_to_minus999_should_be_minus999(self, guessValue):
        result = guess_number.guess_int(-999,-999)
        self.assertEqual(result,-999,"should be -999")

    @mock.patch('random.randint',return_value=int(-167))
    def test_guess_int_minus1000_to_minus1_should_be_minus167(self, guessValue):
        result = guess_number.guess_int(-1000,-1)
        self.assertEqual(result,-167,"should be -167")

    @mock.patch('random.randint',return_value=int(16))
    def test_guess_int_minus99_to_99_should_be_16(self, guessValue):
        result = guess_number.guess_int(-99,99)
        self.assertEqual(result,16,"should be 16")

    #test case float
    @mock.patch('random.uniform',return_value=float(1.6))
    def test_guess_float_0_to_9_should_be_1point6(self, guessValue):
        result = guess_number.guess_float(0,9)
        self.assertEqual(result,1.6,"should be 1.6")
    
    @mock.patch('random.uniform',return_value=float(1.67))
    def test_guess_float_0point1_to_99point9_should_be_1point67(self, guessValue):
        result = guess_number.guess_float(0.1,99.9)
        self.assertEqual(result,1.67,"should be 1.67")

    @mock.patch('random.uniform',return_value=float(167.167))
    def test_guess_float_minus99point999_to_99point999_should_be_minus_167point167(self, guessValue):
        result = guess_number.guess_float(-99.999,99.999)
        self.assertEqual(result,167.167,"should be -167.167")

    @mock.patch('random.uniform',return_value=float(-3.123456789))
    def test_guess_float_minus99_to_minus1_should_be_minus3point123456789(self, guessValue):
        result = guess_number.guess_float(-99,-1)
        self.assertEqual(result,-3.123456789,"should be -3.123456789")

    @mock.patch('random.randint',return_value=float(0.0))
    def test_guess_float_0_to_0_should_be_0point0(self, guessValue):
        result = guess_number.guess_int(0,0)
        self.assertEqual(result,0.0,"should be 0.0")

    @mock.patch('random.randint',return_value=float(-1000.123456))
    def test_guess_float_minus1000point123456_to_minus1000point123456_should_be_minus1000point123456(self, guessValue):
        result = guess_number.guess_int(-1000.123456,-1000.123456)
        self.assertEqual(result,-1000.123456,"should be -1000.123456")

    @mock.patch('random.randint',return_value=float(100.123456))
    def test_guess_float_100point123456_to_100point123456_should_be_100point123456(self, guessValue):
        result = guess_number.guess_int(100.123456,100.123456)
        self.assertEqual(result,100.123456,"should be 100.123456")
