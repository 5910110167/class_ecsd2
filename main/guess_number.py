""" file guess number"""
import random


def guess_int(start, stop):
    """
        test guess_int.
    """
    return random.randint(start, stop)


def guess_float(start, stop):
    """
        test guess_float.
    """
    return random.uniform(start, stop)
